#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include <WinSock2.h>  
#include <stdio.h>  
#include <iostream>   

using namespace std;

#pragma comment(lib, "ws2_32.lib")   

#define UDP_PORT 12895  
#define TCP_PORT 12811 
#define WM_UPDATE_STATIC (WM_USER + 100) 
#define BUFFER_SIZE 8192 
#define FILE_NAME_MAX_SIZE 512 
// SecWin 对话框

class SecWin : public CDialog
{
	DECLARE_DYNAMIC(SecWin)

public:
	SecWin(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~SecWin();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CHATTING_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CListBox infList;
	// 接收普通udp信息的线程
	CWinThread * m_pThread;
	// 接收tcp文件信息的线程
	CWinThread * recvFileThread;
	// 发送tcp文件信息的线程
	CWinThread * sendFileThread;
	static UINT ThreadFunction(LPVOID pParam);
	static UINT RecvFileThread(LPVOID pParam);
	static UINT SendFileThread(LPVOID pParam);
	afx_msg LRESULT OnUpdateStatic(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedSendMsg();
	std::string client_name;
	std::string client_ip;
	CStatic friend_name;
	CFont cfont;
	afx_msg void OnClose();
	CButton btn_recvFile;
	CButton btn_rejectFile;
	afx_msg void OnBnClickedReceiveFile();
	afx_msg void OnBnClickedRejectFile();
	afx_msg void OnBnClickedSendFile();
	void SendIPAndFileName(CString inf);
};
