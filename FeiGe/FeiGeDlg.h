
// FeiGeDlg.h: 头文件
//

#pragma once
#include "afxcmn.h"
#include <WinSock2.h>  
#include <stdio.h>  
#include <iostream>   
#include "afxwin.h"
#include "SecWin.h"
using namespace std;

#pragma comment(lib, "ws2_32.lib")   

#define GET_HOST_COMMAND "GetIPAddr"
const int MAX_BUF_LEN = 255;
#define SERVER_PORT 8849  
#define CLIENT_PORT 8859

// CFeiGeDlg 对话框
class CFeiGeDlg : public CDialog
{
// 构造
public:
	CFeiGeDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FEIGE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持



// 实现
protected:
	HICON m_hIcon;

	// 初始化主窗口
	virtual BOOL OnInitDialog();

	// 接受UDP广播的线程
	static DWORD WINAPI RecvProc(LPVOID lpParameter);
	// 发送UDP广播的线程
	static DWORD WINAPI SendProc(LPVOID lpParameter);

	DECLARE_MESSAGE_MAP()
public:
	
	afx_msg void OnSendText();
	afx_msg void OnBnClickedRefresh();
	
	afx_msg void OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDblclkList1(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnSendToAll();

	afx_msg void OnExit();
	afx_msg void On32774();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
