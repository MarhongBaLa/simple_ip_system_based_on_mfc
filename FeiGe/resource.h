//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 FeiGe.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FEIGE_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       130
#define IDD_CHATTING_DIALOG             131
#define IDD_DIALOG1                     133
#define IDC_LIST1                       1000
#define IDC_REFRESH                     1001
#define IDC_FRIEND_NAME                 1002
#define IDC_SEND_MSG                    1003
#define IDC_HISTORY_LIST                1004
#define IDC_EDIT_MSG                    1005
#define IDC_TEST                        1006
#define IDC_SENDUDP                     1007
#define IDC_HISTORY                     1008
#define IDC_RECEIVE_FILE                1010
#define IDC_REJECT_FILE                 1011
#define IDC_SEND_FILE                   1012
#define IDC_GINF_LIST                   1013
#define IDC_GROUP_NAME                  1014
#define IDC_GROUP_LIST                  1015
#define IDC_EDITG_MSG                   1016
#define IDC_SENDG_MSG                   1017
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_SEND_TEXT                    32775
#define ID_SEND_FILE                    32776
#define ID_EXIT                         32777
#define ID_SEND_TO_ALL                  32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
