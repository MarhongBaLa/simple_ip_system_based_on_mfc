
// FeiGe.h: PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "afxcmn.h"
#include <WinSock2.h>  
#include <stdio.h>  
#include <iostream>   
#include "afxwin.h"
using namespace std;

#pragma comment(lib, "ws2_32.lib")   

// CFeiGeApp:
// 有关此类的实现，请参阅 FeiGe.cpp
//

class CFeiGeApp : public CWinApp
{
public:
	

// 重写
public:
	virtual BOOL InitInstance();

// 实现
protected:
	// 获取本机当前的IP地址
	bool GetLocalIP(char* ip);
	// 接受UDP广播
	bool DoServer();
	DECLARE_MESSAGE_MAP()
};

extern CFeiGeApp theApp;
