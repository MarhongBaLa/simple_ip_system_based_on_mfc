#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include <WinSock2.h>  
#include <stdio.h>  
#include <iostream>   

using namespace std;

#pragma comment(lib, "ws2_32.lib")   

#define GROUP_PORT 12822  

#define WM_UPDATE_GROUP (WM_USER + 100) 



// GroupChatDlg 对话框

class GroupChatDlg : public CDialog
{
	DECLARE_DYNAMIC(GroupChatDlg)

public:
	GroupChatDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~GroupChatDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CStatic group_name;  //显示群的名字
	CListBox group_infList; // 显示群成员发送的消息列表
	CEdit group_Msg;   // 显示在群里发送的消息

	CWinThread * recvThread; // 接收普通udp信息的线程
	CFont cfont2; // 自定义的字体
	CString group_names; // 所有的群成员名字用一个CString来表示，中间用！隔开
	CString group_ips; // 所有的群成员的ip用一个CString来表示，中间用！隔开
	//vector<CString> names; // 将所有的成员名解析到一个vector中
	//vector<CString> ips; // 将所有成员的ip解析到一个vector中
	static UINT RecvUDPThread(LPVOID pParam); 	// 自定义的接收普通udp信息的线程
	afx_msg LRESULT OnUpdateGroup(WPARAM wParam, LPARAM lParam); // 处理接收到的自定义消息
	afx_msg void OnBnClickedSendgMsg();
};
